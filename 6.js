// Luis Sainz

for(let x=1; x<=100; x++){
  const fizz = x%3 == 0
  const buzz = x%5 == 0

  if (fizz && buzz) console.log("fizzBuzz")
  else if (fizz) console.log("fizz")
  else if (buzz) console.log("buzz")
  else 
    console.log(x)
}