// Ulises Vargas

// Un programa que imprima del 1 al 100
// Que en cada multiplo de 3 imprima Fizz
// Para multriplos de 5 que escriba Buzz
// Y para multiplos de 3 y 5 que imprima Fizz Buzz
​
const num = 101
​
for (let i = 1; i < num; i++) {
    if (i % 3 === 0 && i % 5 === 0) console.log('Fizz Buzz')
    else if (i % 3 === 0) console.log('Fizz');
    else if (i % 5 === 0) console.log('Buzz');
    else console.log(i);
}